﻿#pragma checksum "..\..\CreateCACertPopupWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "04D9D178EBEBF526C4FA6E4FE222750F539C54D9"
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace OpenSSLUI {
    
    
    /// <summary>
    /// CreateCACertPopupWindow
    /// </summary>
    public partial class CreateCACertPopupWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 6 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label _PopUpCreateCACertCountryNameLbl;
        
        #line default
        #line hidden
        
        
        #line 7 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox _PopUpCreateCACertCounrtyNameTF;
        
        #line default
        #line hidden
        
        
        #line 8 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label _PopUpCreateCACertStateLbl;
        
        #line default
        #line hidden
        
        
        #line 9 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox _PopUpCreateCACertStateTF;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label _PopUpCreateCACertLocationLbl;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox _PopUpCreateCACertLocationTF;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label _PopUpCreateCACertOrgNameLbl;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox _PopUpCreateCACertOrgNameTF;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label _PopUpCreateCACertOrgUnitLbl;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox _PopUpCreateCACertOrgUnitTF;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label _PopUpCreateCACertCommonNameLbl;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox _PopUpCreateCACertCommonNameTF;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label _PopUpCreateCACertEmailLbl;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox _PopUpCreateCACertEmailTF;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button _PopUpCreateCACertOKBtn;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\CreateCACertPopupWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button _PopUpCreateCACertCancelBtn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/OpenSSLUI;component/createcacertpopupwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\CreateCACertPopupWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this._PopUpCreateCACertCountryNameLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this._PopUpCreateCACertCounrtyNameTF = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this._PopUpCreateCACertStateLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this._PopUpCreateCACertStateTF = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this._PopUpCreateCACertLocationLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this._PopUpCreateCACertLocationTF = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this._PopUpCreateCACertOrgNameLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this._PopUpCreateCACertOrgNameTF = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this._PopUpCreateCACertOrgUnitLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this._PopUpCreateCACertOrgUnitTF = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this._PopUpCreateCACertCommonNameLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this._PopUpCreateCACertCommonNameTF = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this._PopUpCreateCACertEmailLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this._PopUpCreateCACertEmailTF = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this._PopUpCreateCACertOKBtn = ((System.Windows.Controls.Button)(target));
            
            #line 20 "..\..\CreateCACertPopupWindow.xaml"
            this._PopUpCreateCACertOKBtn.Click += new System.Windows.RoutedEventHandler(this._PopUpCreateCACertOKBtn_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this._PopUpCreateCACertCancelBtn = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\CreateCACertPopupWindow.xaml"
            this._PopUpCreateCACertCancelBtn.Click += new System.Windows.RoutedEventHandler(this._PopUpCreateCACertCancelBtn_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

