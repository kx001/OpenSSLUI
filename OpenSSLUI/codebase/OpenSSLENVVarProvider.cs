﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace OpenSSLUI.codebase
{
    class OpenSSLENVVarProvider
    {
        public static String GetCurrentDirPath() 
        {
            //Environmental variable "OPENSSL_UI_PATH"
           
            String _CurrentDirectory = Environment.CurrentDirectory;
            return _CurrentDirectory;
        }

        public static String GetOPENSSLUIPATHEnvVar() 
        {
            String _OpenSSLUIPath = Environment.GetEnvironmentVariable("OPENSSL_UI_PATH");
            //return OpenSSLUI.Properties.Settings.Default.OPENSSL_PATH;
            return _OpenSSLUIPath;
        }
    }
}
